// brought to you by anon and the vim-gang
const AWS = require('aws-sdk');
const fs = require('fs');

// configured for linode: idc if you use something else .. go google or so
const linodeEndpoint = new AWS.Endpoint('eu-central-1.linodeobjects.com');
const s3 = new AWS.S3({
    endpoint: linodeEndpoint,
    accessKeyId: '',
    secretAccessKey: ''
});

module.exports = {

    upload_s3: function (filename, file_prefix, file_content) {

        var uploadParams = {
                Bucket: "",
                Key: '',
                Body: '',
                ACL: "public_read"
        };

        uploadParams.Body = file_content;
        uploadParams.Key = file_prefix + filename;

        // call S3 to retrieve upload file to specified bucket
        s3.upload (uploadParams, function (err, data) {
            if (err) {
                console.log("Error", err);
            } if (data) {
                // maybe return the url... TODO
                console.log("Upload Success", data.Location);
            }
        });
    }
};
