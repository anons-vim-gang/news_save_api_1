// brought to you by anon and the vim-gang
/* API PART */
const express = require("express");
const app = express();
const fetch = require("node-fetch");

//base url prefix of filehost --> probably linode objects
const base_url = 'linodeobjects.com/bucket_contentsblabla';

app.get('/*', async function (req, res, next) {
    let url = req.params[0];
    let filename = await saveArticle(url);
    const new_url = `${base_url}/${filename}.html`;

    res.send(`<html><head><meta http-equiv="refresh" content="5; URL=${new_url}" /><title>Redirecting...</title></head><body><h1 style="min-height: 10em; display: table-cell; vertical-align: middle; text-align: center;">Redirecting...</h1></body></html>`);
    console.log(`GET \t /article \t ${filename} \t ${url}`);

    //article_main(url, filename);
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

/* All the Functions */
const Mercury = require('@postlight/mercury-parser');
const fs = require('fs');
const puppeteer = require('puppeteer');
const slugify = require('slugify');
const sanitize = require('sanitize-filename');
const tools = require('./tools.js');

const user_agent = 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Chrome/88.0.4324.150 Safari/537.36';

async function article_main(url, filename){
              const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser'});
              const page = await browser.newPage();
              await page.setUserAgent(user_agent);
              await page.goto(url, {waitUntil: 'networkidle2'});

              await autoScroll(page);

              await page.pdf({path: filename + '.pdf', format: 'A4'});

              /* const html = await page.content();
              fs.writeFile(filename + '.html', html, function (err) {
                    if (err) return console.log(err);
                    console.log('Writing HTML to ' + filename)
              });*/

              await browser.close();
}

async function autoScroll(page){
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

async function saveArticle(url){
    // parses the article to html file --> saved to disk
    let result = await Mercury.parse(url, {headers: {'User-Agent': user_agent,},});
    console.log(result.title);
    let filename = slugify(sanitize(result.title));
    tools.upload_s3(
        filename + '.html',
        'files/content',
        `<!DOCTYPE html><html lang="en"><head><title>${result.title}</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/spcss@0.6.0"></head><body><div><h1>${result.title}</h1><h3>${result.excerpt}</h3><a href="${result.url}">Original Article</a><br /><a href="${filename + '.pdf'}">View PDF Version</a><p>${result.author}</p><p style="color: grey;">${result.date_published}</p></div>${result.content}</body></html>`
    );
    return filename;
}
